﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public IList<Contacts> MyList
        {
            get;
            private set;
        }

        public MainPage()
        {
            InitializeComponent();
            MyList = new List<Contacts>();
            MyList.Add(new Contacts
            {
                Id = 1,
                Name = "Romuald",
                Address = "Marseille",
                Image ="image1.jpg"
            });
            MyList.Add(new Contacts
            {
                Id = 2,
                Name = "Dan",
                Address = "San Diego",
                Image = "image2.jpg"
            });

            MyList.Add(new Contacts
            {
                Id = 3,
                Name = "William",
                Address = "Seattle",
                Image = "image3.jpg"
            });

            MyList.Add(new Contacts
            {
                Id = 4,
                Name = "Lola",
                Address = "Las Vegas",
                Image = "image4.jpeg"
            });

             MyList.Add(new Contacts
            {
                Id = 5,
                Name = "Francoise",
                Address = "Saint-Tropez",
                Image ="image5.jpg"
            });

            BindingContext = this;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            MyList.Add(new Contacts() { Id = 11, Name = "Student", Address = "Address", Image = "usa.png" });
        }

        private void Delete_Clicked(object sender, EventArgs e)
        {
            MyList.Remove(MyList.LastOrDefault());
        }

        private void MenuItem_Delete(object sender, EventArgs e)
        {
            var mi = sender as MenuItem;
            MyList.Remove((Contacts)mi.CommandParameter);
        }
    }
}
//    <ListView  ItemsSource="{Binding MyList}" IsVisible="True" IsPullToRefreshEnabled="True" RefreshCommand="{Binding RefreshCommand}" IsRefreshing="{Binding IsRefreshing}">  
